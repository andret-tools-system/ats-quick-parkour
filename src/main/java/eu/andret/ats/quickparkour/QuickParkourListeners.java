/*
 * Copyright Andret (c) 2019-2022. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.quickparkour;

import eu.andret.ats.parkour.parkour.Medal;
import eu.andret.ats.parkour.parkour.ParkourGame;
import eu.andret.ats.parkour.player.ParkourPlayer;
import eu.andret.ats.quickparkour.item.ParkourGameItem;
import eu.andret.ats.quickparkour.item.ParkourGameItemData;
import eu.andret.ats.quickparkour.item.ParkourGameItemGlobalData;
import eu.andret.ats.quickparkour.item.QuickParkourItem;
import eu.andret.ats.quickparkour.item.QuickParkourItemObjective;
import eu.andret.ats.quickparkour.task.RequestDataTask;
import lombok.Value;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Value
public class QuickParkourListeners implements Listener {
	QuickParkourPlugin plugin;

	@EventHandler
	public void interact(final PlayerInteractEvent event) {
		if (event.getAction().equals(Action.PHYSICAL)) {
			return;
		}
		if (event.getItem() == null) {
			return;
		}
		if (event.getItem().getItemMeta() == null) {
			return;
		}
		final QuickParkourItem item = new QuickParkourItem(plugin, event.getItem());
		if (QuickParkourItemObjective.MAIN.equals(item.getObjective())) {
			prepareAndExecuteRequest(ParkourGame.Type.SERVER, event.getPlayer());
		}
	}

	@EventHandler
	public void inventoryClick(final InventoryClickEvent event) {
		if (event.getCurrentItem() == null) {
			return;
		}
		if (event.getCurrentItem().getItemMeta() == null) {
			return;
		}
		if (!(event.getWhoClicked() instanceof final Player player)) {
			return;
		}
		final QuickParkourItem item = new QuickParkourItem(plugin, event.getCurrentItem());
		if (item.isDisabled()) {
			event.setCancelled(true);
		}
		if (QuickParkourItemObjective.MAIN.equals(item.getObjective())) {
			prepareAndExecuteRequest(ParkourGame.Type.SERVER, player);
			event.setCancelled(true);
		}
		if (QuickParkourItemObjective.EXIT.equals(item.getObjective())) {
			player.closeInventory();
		}
	}

	@EventHandler
	public void typeClick(final InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		if (event.getCurrentItem() == null || event.getCurrentItem().getType().equals(Material.AIR)) {
			return;
		}
		final ParkourGameItem quickParkourItem = new ParkourGameItem(plugin, event.getCurrentItem());
		final String parkour = quickParkourItem.getParkour();
		if (QuickParkourItemObjective.PARKOUR.equals(quickParkourItem.getObjective()) && parkour != null) {
			final ParkourGame parkourGame = plugin.getParkourPlugin().getParkourManager().getParkour(parkour);
			if (parkourGame == null) {
				return;
			}
			final ParkourPlayer parkourPlayer = plugin.getParkourPlugin().getPlayerManager().getParkourPlayer(player);
			final boolean isVip = plugin.getParkourPlugin().getRankProvider().map(rankProvider -> rankProvider.isVip(player)).orElse(false);
			if (parkourGame.getOptions().isVipOnly() && !isVip) {
				player.sendMessage(plugin.getParkourPlugin().msg("error-no-vip"));
				event.setCancelled(true);
			} else {
				plugin.getParkourPlugin().getPlayerManager().teleportToCheckpoint(parkourPlayer, parkourGame.getCheckpoints().get(0));
			}
			return;
		}
		if (QuickParkourItemObjective.PLAYERS.equals(quickParkourItem.getObjective())) {
			prepareAndExecuteRequest(ParkourGame.Type.PLAYERS, player);
		}
		if (QuickParkourItemObjective.SERVER.equals(quickParkourItem.getObjective())) {
			prepareAndExecuteRequest(ParkourGame.Type.SERVER, player);
		}
		if (QuickParkourItemObjective.TRAINING.equals(quickParkourItem.getObjective())) {
			prepareAndExecuteRequest(ParkourGame.Type.TRAINING, player);
		}
	}

	private void prepareAndExecuteRequest(final ParkourGame.Type type, final Player player) {
		final List<ParkourGame> games = plugin.getParkourPlugin().getParkourManager().getAllGames().stream()
				.filter(ParkourGame::isRunning)
				.filter(parkourGame -> parkourGame.getOptions().getType().equals(type))
				.toList();
		request(createInventory(type, (4 + games.size() / 9) * 9), player, games);
	}

	private Inventory createInventory(final ParkourGame.Type type, final int size) {
		final String inventoryName = plugin.getConfig().getString("items.specific." + type.name().toLowerCase() + ".name");
		final Inventory inventory = plugin.getServer().createInventory(null, size, ChatColor.translateAlternateColorCodes('&', String.valueOf(inventoryName)));
		final int[][] pattern = plugin.getPattern();
		for (int i = 0; i < pattern.length; i++) {
			for (int j = 0; j < pattern[i].length; j++) {
				inventory.setItem(i * 9 + j, plugin.mapPatternElement(pattern[i][j]).toItemStack());
			}
		}
		inventory.setItem(inventory.getSize() - 1, plugin.getExit().toItemStack());
		return inventory;
	}

	private ParkourGameItem createParkourItem(final Player player, final ParkourGame parkour, final ParkourGameItemGlobalData parkourGameItemGlobalData) {
		final ParkourGameItem parkourGameItem = new ParkourGameItem(plugin, new ItemStack(plugin.getColorMapping().getMaterial(parkour.getOptions().getColor())));
		parkourGameItem.setDisabled(true);
		parkourGameItem.hideFlags();
		parkourGameItem.setParkour(parkour.getName());
		parkourGameItem.setObjective(QuickParkourItemObjective.PARKOUR);
		final String name = String.valueOf(plugin.getConfig().getString("items.detailed.parkour.name")).replace("%PARKOUR%", parkour.getDisplayName());
		parkourGameItem.setName(name);
		final Map<String, String> replacementMap = getReplacementMap(player, parkour, parkourGameItemGlobalData);
		final List<String> list = plugin.getConfig().getStringList("items.detailed.parkour.lore")
				.stream()
				.filter(loreLine -> !loreLine.equals("%VIP%") || parkour.getOptions().isVipOnly())
				.map(loreLine -> {
					for (final Map.Entry<String, String> entry : replacementMap.entrySet()) {
						loreLine = loreLine.replace(entry.getKey(), entry.getValue());
					}
					return loreLine;
				})
				.toList();
		if (!replacementMap.get("%DATE%").equals(plugin.getEmptyDatePattern())) {
			parkourGameItem.setGlowing(true);
		}
		parkourGameItem.setLore(list);
		return parkourGameItem;
	}

	private Map<String, String> getReplacementMap(final Player player, final ParkourGame parkour, final ParkourGameItemGlobalData parkourGameItemGlobalData) {
		final Map<String, String> result = new HashMap<>();
		final String vip = "%VIP%";
		final String medal = "%MEDAL%";
		final String countKey = "%COUNT%";
		final String globalTimeKey = "%GLOBAL_TIME%";
		final String personalTimeKey = "%PLAYER_TIME%";
		final String dateKey = "%DATE%";
		result.put(vip, getVipReplacement(player));
		result.put(medal, "none");
		result.put(countKey, "0");
		result.put(dateKey, plugin.getEmptyDatePattern());
		if (parkourGameItemGlobalData == null) {
			result.put(globalTimeKey, plugin.getEmptyTimePattern());
			result.put(personalTimeKey, plugin.getEmptyTimePattern());
			return result;
		}
		if (parkourGameItemGlobalData instanceof final ParkourGameItemData parkourGameItemData) {
			final ParkourGame.Result parkourResult = parkour.getResult(parkourGameItemData.getPersonalTime());
			result.put(medal, Optional.ofNullable(parkourResult.getMedal()).map(Medal::getDisplayName).orElse(""));
			result.put(countKey, String.valueOf(parkourGameItemData.getCount()));
			result.put(globalTimeKey, plugin.getParkourPlugin().formatTime(parkourGameItemData.getGlobalTime()));
			result.put(personalTimeKey, plugin.getParkourPlugin().formatTime(parkourGameItemData.getPersonalTime()));
			result.put(dateKey, parkourGameItemData.getLastComplete());
			return result;
		}
		result.put(globalTimeKey, plugin.getParkourPlugin().formatTime(parkourGameItemGlobalData.getGlobalTime()));
		result.put(personalTimeKey, plugin.getEmptyTimePattern());
		return result;
	}

	private String getVipReplacement(final Player player) {
		return plugin.getParkourPlugin().getRankProvider()
				.map(rankProvider -> rankProvider.isVip(player))
				.filter(Boolean.TRUE::equals)
				.map(ignored -> plugin.getVipAllowed())
				.orElse(plugin.getVipDisallowed());
	}

	private void request(final Inventory inventory, final Player player, final List<ParkourGame> parkourGames) {
		plugin.getParkourPlugin().getConnection()
				.map(connection -> new RequestDataTask(connection, player.getUniqueId(), parkourGames, result -> {
					result.entrySet().stream()
							.map(entry -> createParkourItem(player, entry.getKey(), entry.getValue()))
							.map(QuickParkourItem::toItemStack)
							.forEach(inventory::addItem);
					plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> player.openInventory(inventory));
				}))
				.ifPresent(connection -> plugin.getServer().getScheduler().runTaskAsynchronously(plugin, connection));
	}
}
