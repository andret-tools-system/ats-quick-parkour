/*
 * Copyright Andret (c) 2019-2022. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.quickparkour;

import eu.andret.arguments.AnnotatedCommand;
import eu.andret.arguments.CommandManager;
import eu.andret.ats.parkour.ParkourPlugin;
import eu.andret.ats.parkour.parkour.ParkourGame;
import eu.andret.ats.quickparkour.item.QuickParkourItem;
import eu.andret.ats.quickparkour.item.QuickParkourItemObjective;
import lombok.Getter;
import org.bstats.bukkit.Metrics;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public final class QuickParkourPlugin extends JavaPlugin {
	private static final String DISPLAY = "display";
	private final QuickParkourItem exit = new QuickParkourItem(this, Material.REDSTONE_BLOCK);
	private final QuickParkourItem glass = new QuickParkourItem(this, Material.BLACK_STAINED_GLASS_PANE);
	private final QuickParkourItem mainQuickParkourItem = new QuickParkourItem(this, Material.INK_SAC);
	private final QuickParkourItem serverMapItemStack = new QuickParkourItem(this, Material.APPLE);
	private final QuickParkourItem playersMapItemStack = new QuickParkourItem(this, Material.SKELETON_SKULL);
	private final QuickParkourItem trainingMapItemStack = new QuickParkourItem(this, Material.BOW);
	private final int[][] pattern = {
			{0, 0, 2, 0, 1, 0, 3, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0}
	};
	private final ColorMapping colorMapping = new ColorMapping();

	private String emptyDatePattern;
	private String emptyTimePattern;

	private String vipAllowed;
	private String vipDisallowed;

	@Override
	public void onEnable() {
		saveDefaultConfig();
		setupItems();
		getServer().getPluginManager().registerEvents(new QuickParkourListeners(this), this);
		vipAllowed = getConfig().getString("vip.allowed");
		vipDisallowed = getConfig().getString("vip.disallowed");
		final AnnotatedCommand<QuickParkourPlugin> command = CommandManager.registerCommand(QuickParkourCommand.class, this);
		command.getOptions().setAutoTranslateColors(true);
		command.setOnUnknownSubCommandExecutionListener(sender -> sender.sendMessage("Did you mean /quickparkour get?"));
		command.setOnInsufficientPermissionsListener(sender -> sender.sendMessage("You have no permissions to use that command!"));

		emptyDatePattern = getConfig().getString("placeholder.empty-date");
		emptyTimePattern = getConfig().getString("placeholder.empty-time");

		new Metrics(this, 10700);

		getParkourPlugin().getGameItemMap().setItem(0, mainQuickParkourItem);
		getParkourPlugin().getWorldItemMap().setItem(0, mainQuickParkourItem);
	}

	public ParkourPlugin getParkourPlugin() {
		return getPlugin(ParkourPlugin.class);
	}

	private void setupItems() {
		setupQuickParkourItem(serverMapItemStack, ParkourGame.Type.SERVER, QuickParkourItemObjective.SERVER);
		setupQuickParkourItem(playersMapItemStack, ParkourGame.Type.PLAYERS, QuickParkourItemObjective.PLAYERS);
		setupQuickParkourItem(trainingMapItemStack, ParkourGame.Type.TRAINING, QuickParkourItemObjective.TRAINING);

		glass.setName(null);
		glass.setLore(null);
		glass.setObjective(QuickParkourItemObjective.GLASS);
		glass.setDisabled(true);
		glass.hideFlags();

		exit.setName(getConfig().getString("items.general.exit.name"));
		exit.setLore(getConfig().getStringList("items.general.exit.lore"));
		exit.setObjective(QuickParkourItemObjective.EXIT);
		exit.setDisabled(true);
		exit.hideFlags();

		mainQuickParkourItem.setName(getConfig().getString("items.general.main.name"));
		mainQuickParkourItem.setLore(getConfig().getStringList("items.general.main.lore"));
		mainQuickParkourItem.setObjective(QuickParkourItemObjective.MAIN);
		mainQuickParkourItem.setDisabled(true);
		mainQuickParkourItem.hideFlags();
	}

	private void setupQuickParkourItem(final QuickParkourItem item, final ParkourGame.Type type, final QuickParkourItemObjective objective) {
		final String path = "items.specific." + type.name().toLowerCase();
		item.setName(getConfig().getString(path + ".name"));
		item.setLore(getConfig().getStringList(path + ".lore"));
		item.setObjective(objective);
		item.setDisabled(true);
		item.hideFlags();
	}

	public QuickParkourItem mapPatternElement(final int element) {
		return switch (element) {
			case 1 -> serverMapItemStack;
			case 2 -> playersMapItemStack;
			case 3 -> trainingMapItemStack;
			default -> glass;
		};
	}
}
