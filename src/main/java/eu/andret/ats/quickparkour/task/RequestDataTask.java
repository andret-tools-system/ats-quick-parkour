/*
 * Copyright Andret (c) 2019-2022. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.quickparkour.task;

import eu.andret.ats.parkour.parkour.ParkourGame;
import eu.andret.ats.parkour.tasks.database.AbstractTask;
import eu.andret.ats.quickparkour.item.ParkourGameItemData;
import eu.andret.ats.quickparkour.item.ParkourGameItemGlobalData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

public class RequestDataTask extends AbstractTask {
	private final UUID uuid;
	private final List<ParkourGame> parkourGames;
	private final Consumer<Map<ParkourGame, ParkourGameItemGlobalData>> callback;

	public RequestDataTask(final Connection connection, final UUID uuid, final List<ParkourGame> parkourGames, final Consumer<Map<ParkourGame, ParkourGameItemGlobalData>> callback) {
		super(connection);
		this.uuid = uuid;
		this.parkourGames = parkourGames;
		this.callback = callback;
	}

	@Override
	public void run() {
		final Map<String, ParkourGameItemGlobalData> personalData = getPersonalData();
		final Map<String, ParkourGameItemGlobalData> globalData = getGlobalData();
		callback.accept(recalculate(personalData, globalData));
	}

	private Map<String, ParkourGameItemGlobalData> getPersonalData() {
		try (final PreparedStatement stat = connection.prepareStatement(
				"SELECT parkour, uuid, MIN(duration) AS personal_time, COUNT(*) AS count, MAX(date) AS last_complete, " +
						"(SELECT MIN(duration) FROM ats_parkour_records AS r2 WHERE r1.parkour = r2.parkour) AS global_time " +
						"FROM ats_parkour_records AS r1 WHERE uuid = ? GROUP BY parkour;")) {
			stat.setString(1, uuid.toString());
			final ResultSet rs = stat.executeQuery();
			final Map<String, ParkourGameItemGlobalData> result = new LinkedHashMap<>();
			while (rs.next()) {
				final String parkour = rs.getString("parkour");
				result.put(parkour, new ParkourGameItemData(rs.getDouble("global_time"), rs.getInt("count"), rs.getFloat("personal_time"), rs.getString("last_complete")));
			}
			return result;
		} catch (final Exception ex) {
			ex.printStackTrace();
		}
		return Collections.emptyMap();
	}

	private Map<String, ParkourGameItemGlobalData> getGlobalData() {
		try (final PreparedStatement stat = connection.prepareStatement(
				"SELECT parkour, MIN(duration) AS global_time FROM ats_parkour_records WHERE uuid <> ? GROUP BY parkour")) {
			stat.setString(1, uuid.toString());
			final ResultSet rs = stat.executeQuery();
			final Map<String, ParkourGameItemGlobalData> result = new LinkedHashMap<>();
			while (rs.next()) {
				final String parkour = rs.getString("parkour");
				result.put(parkour, new ParkourGameItemGlobalData(rs.getDouble("global_time")));
			}
			return result;
		} catch (final Exception ex) {
			ex.printStackTrace();
		}
		return Collections.emptyMap();
	}

	private Map<ParkourGame, ParkourGameItemGlobalData> recalculate(final Map<String, ParkourGameItemGlobalData> personalData, final Map<String, ParkourGameItemGlobalData> globalData) {
		final Map<ParkourGame, ParkourGameItemGlobalData> result = new LinkedHashMap<>();
		for (final ParkourGame parkourGame : parkourGames) {
			result.put(parkourGame, personalData.getOrDefault(parkourGame.getName(), globalData.getOrDefault(parkourGame.getName(), null)));
		}
		return result;
	}
}
